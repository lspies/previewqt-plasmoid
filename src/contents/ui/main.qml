import QtQuick
import QtQuick.Layouts
import QtMultimedia

import org.kde.plasma.plasmoid
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents
import org.kde.kirigami as Kirigami
import org.kde.plasma.plasma5support as Plasma5Support

PlasmoidItem {
    id: previewqt

    width: Kirigami.Units.gridUnit * 15
    height: Kirigami.Units.gridUnit * 15

    Plasmoid.backgroundHints: "NoBackground";

    // some information about current file
    property bool isCurrentAnimated: false
    property bool isCurrentVideo: false
    property string currentSourceFile: ""
    property int currentFileIndex: 0
    property int currentFileCount: 0
    property string currentFileName: ""
    property string currentMime: ""
    property string previewSource: ""

    // check for basic enough version
    property string errorString: ""

    // the required minimum version of this file
    property real minVersion: 1.0

    property string previewQtExecutable
    onPreviewQtExecutableChanged: {
        exec(previewqt.previewQtExecutable + " -v")
    }

    // just exeucte, no processing
    Plasma5Support.DataSource {
        id: onlyLaunchExec
        engine: "executable"
        connectedSources: []
        onNewData: (sourceName, data) => {
            disconnectSource(sourceName)
        }
    }

    Timer {
        id: recheckPreviewQt
        interval: 2000
        running: previewqt.errorString != ""
        repeat: true
        onTriggered: {
            // we need to add the timestamp as using the same command repeatedly does not lead to executionm
            // this can be the case if the executable is changed to something wrong and back to the right string from before
            var date = new Date()
            exec(previewqt.previewQtExecutable + " -v " + date.getTime())
        }
    }

    // execute and process output
    Plasma5Support.DataSource {
        id: executeSource
        engine: "executable"
        connectedSources: []
        onNewData: (sourceName, data) => {

            // only the version should be checked
            if(sourceName.endsWith("-v")) {
                if(data["stdout"] == "") {
                    previewqt.errorString = "Error: PreviewQt was not found."
                } else {
                    var curVer = data["stdout"].split("PreviewQt ")[1].split("\n")[0]*1.0
                    if(curVer < minVersion)
                        previewqt.errorString = "Error: The installed PreviewQt version is too old."
                    else
                        previewqt.errorString = ""
                }
                return
            }

            // read output
            var ret = data["stdout"].split("\n")
            if(ret.length == 1) return

            // if we got here, then there has been no error
            previewqt.errorString = ""

            // loop over output
            for(var i in ret) {

                var cur = ret[i]

                /*if(cur.startsWith("display mime:"))
                    ret_disp = cur.split("display mime: ")[1]
                else */if(cur.startsWith("source mime:"))
                    previewqt.currentMime = cur.split("source mime: ")[1]
                else if(cur.startsWith("tmp path:")) {
                    previewqt.previewSource = ""
                    previewqt.previewSource = cur.split("tmp path: ")[1]
                } else if(cur.startsWith("file index:"))
                    previewqt.currentFileIndex = cur.split("file index: ")[1]*1
                else if(cur.startsWith("file count:"))
                    previewqt.currentFileCount = cur.split("file count: ")[1]*1
                else if(cur.startsWith("animated:"))
                    previewqt.isCurrentAnimated = (cur.split("animated: ")[1] == "yes")
                else if(cur.startsWith("video:"))
                    previewqt.isCurrentVideo = (cur.split("video: ")[1] == "yes")
                else if(cur.startsWith("file name:"))
                    previewqt.currentFileName = cur.split("file name: ")[1]
            }

            disconnectSource(sourceName)

        }
    }

    // load new file/page
    onCurrentFileIndexChanged: {
        exec(previewqt.previewQtExecutable + " \"%1\" --process-only --file-num %2".arg(previewqt.currentSourceFile).arg(previewqt.currentFileIndex))
    }

    // execute something
    function exec(cmd, onlylaunch=false) {
        if(onlylaunch)
            onlyLaunchExec.connectSource(cmd)
        else
            executeSource.connectSource(cmd)
    }

    compactRepresentation: MouseArea {

        id: representation

        Layout.minimumWidth: Plasmoid.formFactor !== PlasmaCore.Types.Vertical ? representation.height : Kirigami.Units.gridUnit
        Layout.minimumHeight: Plasmoid.formFactor === PlasmaCore.Types.Vertical ? representation.width : Kirigami.Units.gridUnit

        property bool wasExpanded

        activeFocusOnTab: true
        hoverEnabled: true

        onPressed: wasExpanded = previewqt.expanded
        onClicked: previewqt.expanded = !wasExpanded

        Kirigami.Icon {
            anchors.fill: parent
            anchors.margins: 5
            source: "org.previewqt.PreviewQt"
        }

        DropArea {
            anchors.fill: parent
            onDropped: (drag) => {
                if(errorString != "") return
                resetInterface()
                exec(previewqt.previewQtExecutable + " \"%1\" --process-only".arg(drag.text))
                previewqt.currentSourceFile = drag.text.split("file:/")[1]
                previewqt.expanded = !previewqt.expanded
            }
        }

    }

    fullRepresentation: Rectangle {

        id: full_deleg

        // some basic layout size limits
        Layout.minimumWidth: Kirigami.Units.gridUnit * 5
        Layout.maximumWidth: Kirigami.Units.gridUnit * 80
        Layout.minimumHeight: Kirigami.Units.gridUnit * 5
        Layout.maximumHeight: Kirigami.Units.gridUnit * 80

        // background color
        color: "#88000000"

        // used to adjust sourceSize with delay
        property int currentSourceWidth: 0
        property int currentSourceHeight: 0

        onWidthChanged: resetSourceSize.restart()
        onHeightChanged: resetSourceSize.restart()

        Timer {
            id: resetSourceSize
            interval: 200
            running: true
            onTriggered: {
                parent.currentSourceWidth = full_deleg.width
                parent.currentSourceHeight = full_deleg.height
            }
        }

        // A standard image display
        Image {
            cache: false
            anchors.fill: parent
            visible: !previewqt.isCurrentAnimated
            mipmap: false
            smooth: false
            source: (previewqt.isCurrentAnimated||previewqt.isCurrentVideo||previewqt.previewSource=="") ? "" : ("file:/" + previewqt.previewSource)
            sourceSize: Qt.size(parent.currentSourceWidth, currentSourceHeight)
            fillMode: Image.PreserveAspectFit
            autoTransform: true
        }

        // an animated image display
        AnimatedImage {
            cache: false
            anchors.fill: parent
            mipmap: false
            smooth: false
            visible: previewqt.isCurrentAnimated
            source: previewqt.isCurrentAnimated ? ("file:/" + previewqt.previewSource) : ""
            sourceSize: Qt.size(parent.currentSourceWidth, currentSourceHeight)
            fillMode: Image.PreserveAspectFit
            autoTransform: true
        }

        // a video display
        Video {
            id: thevideo
            anchors.fill: parent
            property bool isPlaying: thevideo.playbackState==MediaPlayer.PlayingState
            visible: previewqt.isCurrentVideo
            source: previewqt.isCurrentVideo ? ("file:/" + previewqt.previewSource) : ""
            onSourceChanged: {
                play()
            }
        }

        // show the current filename near the top edge
        Rectangle {
            x: (parent.width-width)/2
            y: 10
            width: full_deleg.width
            height: fnametxt.height+10
            color: "#44000000"
            visible: previewqt.previewSource!=""
            opacity: mainmouse.containsMouse||prevmouse.containsMouse||nextmouse.containsMouse||openmouse.containsMouse ? 1 : 0
            Behavior on opacity { NumberAnimation { duration: 200 } }
            radius: 5
            Text {
                id: fnametxt
                x: 5
                y: 5
                width: full_deleg.width-10
                elide: Text.ElideMiddle
                horizontalAlignment: Text.AlignHCenter
                color: "white"
                font.bold: true
                text: previewqt.currentFileName
            }
        }

        // status information unless a file is loaded
        Rectangle {
            anchors.centerIn: parent
            width: statustxt.width+10
            height: statustxt.height+10
            color: "#44000000"
            visible: statustxt.text!="" || errorString!=""
            radius: 5
            Text {
                id: statustxt
                x: 5
                y: 5
                Layout.maximumWidth: parent.parent.width
                elide: Text.ElideMiddle
                color: errorString!="" ? "red" : "white"
                font.bold: true
                text: previewqt.errorString!="" ?
                        previewqt.errorString :
                            (previewqt.currentSourceFile=="" ?
                                    "Drop a file to preview" :
                                    (previewqt.previewSource=="" ?
                                        "Loading..." :
                                        ""))
            }
        }

        // the main mouse area used to listen for double clicks
        MouseArea {
            id: mainmouse
            anchors.fill: parent
            hoverEnabled: true
            onDoubleClicked: {
                passOnToPreviewQt()
            }
        }

        // navigate through document/archive
        Row {
            x: (parent.width-width)/2
            y: (parent.height-height-20)
            Rectangle {
                width: 40
                height: 40
                visible: previewqt.currentFileCount>0
                color: prevmouse.containsMouse ? "#bb000000" : "#44000000"
                Behavior on color { ColorAnimation { duration: 200 } }
                opacity: prevmouse.containsMouse||nextmouse.containsMouse||openmouse.containsMouse||resetmouse.containsMouse ? 1 : 0.2
                Behavior on opacity { NumberAnimation { duration: 200 } }
                radius: 5
                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "<"
                    color: "white"
                    font.bold: true
                    font.pointSize: 12
                }
                MouseArea {
                    id: prevmouse
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onClicked: previewqt.currentFileIndex = (previewqt.currentFileIndex+previewqt.currentFileCount-1)%previewqt.currentFileCount
                }
            }
            Rectangle {
                width: 40
                height: 40
                visible: previewqt.currentFileCount>0
                color: nextmouse.containsMouse ? "#bb000000" : "#44000000"
                Behavior on color { ColorAnimation { duration: 200 } }
                opacity: prevmouse.containsMouse||nextmouse.containsMouse||openmouse.containsMouse||resetmouse.containsMouse ? 1 : 0.2
                Behavior on opacity { NumberAnimation { duration: 200 } }
                radius: 5
                Text {
                    anchors.fill: parent
                    color: "white"
                    font.bold: true
                    font.pointSize: 12
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: ">"
                }
                MouseArea {
                    id: nextmouse
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onClicked: previewqt.currentFileIndex = (previewqt.currentFileIndex+1)%previewqt.currentFileCount
                }
            }
            Rectangle {
                width: 40
                height: 40
                visible: previewqt.isCurrentVideo
                color: playmouse.containsMouse ? "#bb000000" : "#44000000"
                Behavior on color { ColorAnimation { duration: 200 } }
                opacity: playmouse.containsMouse||openmouse.containsMouse||resetmouse.containsMouse ? 1 : 0.2
                Behavior on opacity { NumberAnimation { duration: 200 } }
                radius: 5
                Text {
                    anchors.fill: parent
                    color: "white"
                    font.bold: true
                    font.pointSize: 12
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: thevideo.isPlaying ? "||" : ">"
                }
                MouseArea {
                    id: playmouse
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onClicked: thevideo.isPlaying ? thevideo.pause() : thevideo.play()
                }
            }
            Rectangle {
                width: 40
                height: 40
                visible: previewqt.previewSource!=""
                color: openmouse.containsMouse ? "#bb000000" : "#44000000"
                Behavior on color { ColorAnimation { duration: 200 } }
                opacity: prevmouse.containsMouse||nextmouse.containsMouse||openmouse.containsMouse||resetmouse.containsMouse ? 1 : 0.2
                Behavior on opacity { NumberAnimation { duration: 200 } }
                radius: 5
                Kirigami.Icon {
                    anchors.fill: parent
                    anchors.margins: 5
                    source: "document.open.svg"
                }
                MouseArea {
                    id: openmouse
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        passOnToPreviewQt()
                    }
                }
            }
            Rectangle {
                width: 40
                height: 40
                visible: previewqt.previewSource!=""
                color: resetmouse.containsMouse ? "#bb000000" : "#44000000"
                Behavior on color { ColorAnimation { duration: 200 } }
                opacity: prevmouse.containsMouse||nextmouse.containsMouse||openmouse.containsMouse||resetmouse.containsMouse ? 1 : 0.2
                Behavior on opacity { NumberAnimation { duration: 200 } }
                radius: 5
                Text {
                    anchors.fill: parent
                    color: "red"
                    font.bold: true
                    font.pointSize: 12
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "x"
                }
                MouseArea {
                    id: resetmouse
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        resetInterface()
                    }
                }
            }
        }

        // it is possible to drop a file on the plasmoid for processing
        DropArea {
            anchors.fill: parent
            onDropped: (drag) => {
                if(errorString != "") return
                resetInterface()
                exec(previewqt.previewQtExecutable + " \"%1\" --process-only".arg(drag.text))
                previewqt.currentSourceFile = drag.text.split("file://")[1]
                previewqt.expanded = !previewqt.expanded
            }
        }

    }

    function passOnToPreviewQt() {
        if(previewqt.previewSource == "")
            exec(previewqt.previewQtExecutable, true)
        else {
            if(previewqt.currentFileIndex == -1)
                exec(previewqt.previewQtExecutable + " \"%1\"".arg(previewqt.currentSourceFile), true)
                else {
                    exec(previewqt.previewQtExecutable + " \"%1\" --file-num %2".arg(previewqt.currentSourceFile).arg(previewqt.currentFileIndex), true)
                }
        }
    }

    // reset some of the variables
    function resetInterface() {
        previewqt.currentFileIndex = 0
        previewqt.currentFileCount = 0
        previewqt.currentMime = ""
        previewqt.previewSource = ""
        previewqt.currentSourceFile = ""
        previewqt.isCurrentAnimated = false
        previewqt.isCurrentVideo = false
        previewqt.currentFileName = ""
    }

    // check the version at startup
    Component.onCompleted: {
        // for some reasong the cfg string is not set up yet with a 'normal' property binding
        previewqt.previewQtExecutable = Qt.binding(function() { return plasmoid.configuration.previewQtExecutable })
    }

}
