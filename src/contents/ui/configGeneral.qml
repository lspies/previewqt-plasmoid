/*
    SPDX-FileCopyrightText: 2013 David Edmundson <davidedmundson@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick
import QtQuick.Controls as Controls
import org.kde.kirigami as Kirigami
import org.kde.kcmutils as KCM

KCM.SimpleKCM {

    property alias cfg_previewQtExecutable: setPreviewQtExecutable.text

    Kirigami.FormLayout {
        Controls.TextField {
            id: setPreviewQtExecutable
            Kirigami.FormData.label: i18n("Executable:")
            placeholderText: i18n("PreviewQt executable")
        }
    }
}
