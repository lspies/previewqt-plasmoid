# PreviewQt Plasmoid

PreviewQt Plasmoid is a companion application to **PreviewQt** embedding neatly into the panel or the desktop of **KDE Plasma**. It allows for a easy and fast way to preview a wide range of different files.

Visit the official website of PreviewQt at https://previewqt.org/

## Features

This is the companion plasmoid to **PreviewQt** which is required to be installed. It embeds neatly into the panel or fits as widget on the desktop. Dragging any supported file on the plasmoid shows a preview of the image. **Double clicking the preview opens up PreviewQt** allowing for further interaction.

Supported file formats include:

- More than **140 image formats** - A list of all possibly supported image formats [can be found on the website for PhotoQt](https://photoqt.org/formats)
- All of the common **video formats**
- **PDF documents**, **Archives** (zip, tar.gz, rar, 7-zip), and **Comic Books** (cbz, cbt, cbr, cb7)

Once a preview is passed on to PreviewQt, some more specialized formats and actions are supported:

- **Photo spheres** and **360 degree panoramas** (that use equirectangular prohections)
- **Motion photos**, **Micro videos**, and **Apple live photos**
- **Page/content navigation** for PDF documents, archives, and comic books


## Download and Install

PreviewQt Plasmoid is available from the KDE store.


## Contributing to PreviewQt

The best way to contribute is to give feedback about this plasmoid. To do so, send me an email or open an issue on GitLab: https://gitlab.com/luspi/previewqt-plasmoid/-/issues/new.

If you want to support PreviewQt financially, please **consider donating to the humanitarian relief in Ukraine** instead, for example to the [Ukrainian Red Cross](https://go.luspi.de/ukraine).


## License

PreviewQt Plasmoid is released under the [GPLv2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) (or later) license.
